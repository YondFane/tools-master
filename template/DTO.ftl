package ${packageModule}.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import java.io.Serializable;
import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
<#if hasTimestamp>
import java.sql.Timestamp;
</#if>
<#if hasBigDecimal>
import java.math.BigDecimal;
</#if>

/**
*
* @description ${tableComment}-DTO
* @author ${author}
* @date ${date}
**/
@Setter
@Getter
@ToString
public class ${className} implements Serializable {
<#if columns??>
    <#list columns as column>

    /**
     *  ${column.remark}
     */
    <#if column.remark != ''>
    @ApiModelProperty(value = "${column.remark}")
    <#else>
    @ApiModelProperty(value = "${column.changeColumnName}")
    </#if>
    <#if column.columnType == 'Long'>
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    </#if>
    private ${column.columnType} ${column.changeColumnName};
    </#list>
</#if>

}
