package ${packageModule}.dao;

import ${packageModule}.entity.${camelCaseTableName}Entity;
import ${packageModule}.dao.${camelCaseTableName}Dao;
import org.springframework.stereotype.Repository;

/**
*
* @description ${tableComment}-持久化层接口实现
* @author ${author}
* @date ${date}
**/
@Repository
public class ${className} implements ${camelCaseTableName}Dao {


}
