package ${packageModule}.controller;

import ${packageModule}.request.${camelCaseTableName}GetRequest;
import ${packageModule}.request.${camelCaseTableName}SaveRequest;
import ${packageModule}.request.${camelCaseTableName}PageRequest;
import ${packageModule}.request.${camelCaseTableName}ListRequest;
import ${packageModule}.dto.${camelCaseTableName}DTO;
import ${packageModule}.service.${camelCaseTableName}Service;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
*
* @description ${tableComment}-持久化层接口
* @author ${author}
* @date ${date}
**/
@Slf4j
@Api(tags = "${tableComment}接口")
@RestController
@RequestMapping("/${lowerCamelCaseTableName}")
public class ${className} {

    @Autowired
    private ${camelCaseTableName}Service ${lowerCamelCaseTableName}Service;

    @GetMapping("/get")
    @ApiOperation(value = "单个查询", httpMethod = "POST", notes = "单个查询")
    public ${camelCaseTableName}DTO get(${camelCaseTableName}GetRequest request){

        return ${lowerCamelCaseTableName}Service.get(request);
    }

    @PostMapping("/save")
    @ApiOperation(value = "保存", httpMethod = "POST", notes = "保存")
    public ${camelCaseTableName}DTO save(@RequestBody ${camelCaseTableName}SaveRequest request){

        return ${lowerCamelCaseTableName}Service.save(request);
    }

    @PostMapping("/submit")
    @ApiOperation(value = "提交", httpMethod = "POST", notes = "提交")
    public ${camelCaseTableName}DTO submit(@RequestBody ${camelCaseTableName}SaveRequest request){

        return ${lowerCamelCaseTableName}Service.submit(request);
    }

    @GetMapping("/page")
    @ApiOperation(value = "分页查询", httpMethod = "POST", notes = "分页查询")
    public Object page(${camelCaseTableName}PageRequest request){

        return ${lowerCamelCaseTableName}Service.page(request);
    }

    @GetMapping("/list")
    @ApiOperation(value = "列表查询", httpMethod = "POST", notes = "列表查询")
    public List<${camelCaseTableName}DTO> list(${camelCaseTableName}ListRequest request){

        return ${lowerCamelCaseTableName}Service.list(request);
    }
}
