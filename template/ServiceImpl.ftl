package ${packageModule}.service;

import ${packageModule}.request.${camelCaseTableName}GetRequest;
import ${packageModule}.request.${camelCaseTableName}SaveRequest;
import ${packageModule}.request.${camelCaseTableName}PageRequest;
import ${packageModule}.request.${camelCaseTableName}ListRequest;
import ${packageModule}.dto.${camelCaseTableName}DTO;
import ${packageModule}.entity.${camelCaseTableName}Entity;
import ${packageModule}.dao.${camelCaseTableName}Dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
*
* @description ${tableComment}-业务层接口实现
* @author ${author}
* @date ${date}
**/
@Service
public class ${className} implements ${camelCaseTableName}Service {

    @Autowired
    private ${camelCaseTableName}Dao ${lowerCamelCaseTableName}Dao;

    /**
    * 单个查询
    * @param request
    * @return
    */
    @Override
    public ${camelCaseTableName}DTO get(${camelCaseTableName}GetRequest request) {

        return null;
    }

    /**
    * 保存
    * @param request
    * @return
    */
    @Override
    public ${camelCaseTableName}DTO save(${camelCaseTableName}SaveRequest request) {

        return null;
    }

    /**
    * 提交
    * @param request
    * @return
    */
    @Override
    public ${camelCaseTableName}DTO submit(${camelCaseTableName}SaveRequest request) {

        return null;
    }

    /**
    * 分页查询
    * @param request
    * @return
    */
    @Override
    public Object page(${camelCaseTableName}PageRequest request)  {

        return null;
    }

    /**
    * 列表查询
    * @param request
    * @return
    */
    @Override
    public List<${camelCaseTableName}DTO> list(${camelCaseTableName}ListRequest request)  {

        return null;
    }

}
