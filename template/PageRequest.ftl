package ${packageModule}.request;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import java.io.Serializable;
import io.swagger.annotations.ApiModelProperty;
<#if hasWhereIsDeleted>
import org.hibernate.annotations.Where;
</#if>
<#if hasTimestamp>
import java.sql.Timestamp;
</#if>
<#if hasBigDecimal>
import java.math.BigDecimal;
</#if>
<#if auto>
import org.hibernate.annotations.GenericGenerator;
</#if>


/**
*
* @description ${tableComment}-PageRequest
* @author ${author}
* @date ${date}
**/
@Setter
@Getter
@ToString
public class ${className} implements Serializable {

    /**
    * pageNum 页号
    */
    @ApiModelProperty("页号")
    private Integer pageNum;

    /**
    * pageSize 页行数
    */
    @ApiModelProperty("页行数")
    private Integer pageSize;

<#if columns??>
    <#list columns as column>

    /**
     *  ${column.remark}
     */
    <#if column.remark != ''>
    @ApiModelProperty(value = "${column.remark}")
    <#else>
    @ApiModelProperty(value = "${column.changeColumnName}")
    </#if>
    private ${column.columnType} ${column.changeColumnName};
    </#list>
</#if>

}
