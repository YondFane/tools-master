package ${packageModule}.service;

import ${packageModule}.request.${camelCaseTableName}GetRequest;
import ${packageModule}.request.${camelCaseTableName}SaveRequest;
import ${packageModule}.request.${camelCaseTableName}PageRequest;
import ${packageModule}.request.${camelCaseTableName}ListRequest;
import ${packageModule}.dto.${camelCaseTableName}DTO;
import ${packageModule}.entity.${camelCaseTableName}Entity;

import java.util.List;

/**
*
* @description ${tableComment}-业务层接口
* @author ${author}
* @date ${date}
**/
public interface ${className} {

    /**
    * 单个查询
    * @param request
    * @return
    */
    ${camelCaseTableName}DTO get(${camelCaseTableName}GetRequest request);

    /**
    * 保存
    * @param request
    * @return
    */
    ${camelCaseTableName}DTO save(${camelCaseTableName}SaveRequest request);

    /**
    * 提交
    * @param request
    * @return
    */
    ${camelCaseTableName}DTO submit(${camelCaseTableName}SaveRequest request);

    /**
    * 分页查询
    * @param request
    * @return
    */
    Object page(${camelCaseTableName}PageRequest request);

    /**
    * 列表查询
    * @param request
    * @return
    */
    List<${camelCaseTableName}DTO> list(${camelCaseTableName}ListRequest request);
}
