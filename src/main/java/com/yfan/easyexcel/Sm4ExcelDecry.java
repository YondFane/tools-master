package com.yfan.easyexcel;

import cn.hutool.core.date.DateUtil;
import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.yfan.utils.SM4CBCUtils;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Sm4 国密算法 批量解密
 *
 * @ClassName: Sm4ExcelDecry
 * @Description:
 * @Author: yangfan
 * @Date: 2024-02-26 14:17
 **/
@Slf4j
public class Sm4ExcelDecry {

    public static String PATH = "C:\\Users\\YFAN\\Desktop\\path";
    public static String SAVE_PATH = "C:\\Users\\YFAN\\Desktop\\savePath";


    public static void main(String[] args) {
        File workSpace = new File(PATH);
        if (!workSpace.exists()) {
            System.out.println("目录不存在！");
            return;
        }
        File[] listFiles = new File[1];
        if (workSpace.isDirectory()) {
            listFiles = workSpace.listFiles();
        } else {
            listFiles[0] = workSpace;
        }

        for (File listFile : listFiles) {
            List<List<Object>> dataList = new ArrayList<>();
            List<List<String>> headList = new ArrayList<>();

            EasyExcel.read(listFile.getPath(), new AnalysisEventListener<Map<Integer, String>>() {

                @Override
                public void invokeHeadMap(Map<Integer, String> headMap, AnalysisContext context) {
                    int size = headMap.size();
                    for (int i = 0; i < size; i++) {
                        List<String> list = new ArrayList<>();
                        list.add(headMap.get(i));
                        headList.add(list);
                    }
                }

                @Override
                public void invoke(Map<Integer, String> data, AnalysisContext context) {
                    int size = data.size();
                    List<Object> list = new ArrayList<>();
                    System.out.println(data);
                    for (int i = 0; i < size; i++) {
                        String content = data.get(i);
                        try {
                            // 需要格式化时间的列
                            List<Integer> integerList = Arrays.stream(new Integer[]{}).collect(Collectors.toList());
                            if (integerList.contains(i)) {
                                list.add(DateUtil.date(Long.parseLong(content.replace(",", ""))).toString());
                                continue;
                            }
                            // 空值处理
                            if (content == null || "".equals(content) || "[NULL]".equals(content)) {
                                list.add("");
                                continue;
                            }

                            // 解密处理
                            list.add(SM4CBCUtils.decrypt(data.get(i)));
                        } catch (Exception e) {
//                            log.error("异常报错", e);
                            // 非加密数据
                            list.add(data.get(i));
                        }
                    }
                    System.out.println(list);
                    dataList.add(list);
                }

                @Override
                public void doAfterAllAnalysed(AnalysisContext context) {
                }
            }).doReadAll();

            /*for (List<Object> list : dataList) {
                String chouQuTime = chouQuTimeMap.get(list.get(7));
                if (chouQuTime != null) {
                    list.remove(0);
                    list.add(0, chouQuTime);
                }
            }*/

            String fileName = PATH + "/" + "解密后-" + listFile.getName();
            if (!workSpace.isDirectory()) {
                fileName = SAVE_PATH + "/" + "解密后-" + listFile.getName();
            }
            File file = new File(fileName);
            if (file.exists()) {
                file.delete();
            }
            EasyExcel.write(fileName).head(headList).sheet("模板").doWrite(dataList);
        }
    }

}
