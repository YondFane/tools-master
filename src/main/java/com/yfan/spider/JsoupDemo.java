package com.yfan.spider;


import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;

/**
 * Jsoup爬虫Demo
 * @ClassName: JsoupDemo
 * @Description:
 * @Author: yangfan
 * @Date: 2024-12-13 11:08
 **/
public class JsoupDemo {
    public static void main(String[] args) {
        String url = "https://www.baidu.com"; // 替换为你要爬取的网站URL
        try {
            // 获取网页文档
            Document document = Jsoup.connect(url).get();

            // 打印网页标题
            System.out.println("Title: " + document.title());

            // 提取所有的链接
            Elements links = document.select("a[href]");
            for (Element link : links) {
                System.out.println("Link: " + link.attr("href"));
                System.out.println("Text: " + link.text());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
