package com.yfan.enums;

/**
 * 模板文件类型
 *
 * @Author: YFAN
 * @CreateTime: 2025-01-02 11:20
 */
public enum TemplateEnum {

    ENTITY("Entity.ftl", "实体类", "entity"),
    DTO("DTO.ftl", "实体转换类", "dto"),
    GET_REQUEST("GetRequest.ftl", "查询请求类", "request"),
    SAVE_REQUEST("SaveReqest.ftl", "保存请求类", "request"),
    PAGE_REQUEST("PageRequest.ftl", "分页请求类", "request"),
    LIST_REQUEST("ListRequest.ftl", "列表请求类", "request"),
    SERVICE("Service.ftl", "业务接口类", "service"),
    SERVICE_IMPL("ServiceImpl.ftl", "业务接口实现类", "service"),
    DAO("Dao.ftl", "持久化接口类", "dao"),
    DAO_IMPL("DaoImpl.ftl", "持久化接口实现类", "dao"),
    CONTROLLER("Controller.ftl", "控制层类", "controller"),
    REPOSITORY("Repository.ftl", "持久化接口实现类", "repository"),
    ;

    // 模板文件名称
    private String code;
    // 模板文件描述
    private String des;
    // 模板文件目录
    private String directory;

    private TemplateEnum(String code, String des, String directory) {
        this.code = code;
        this.des = des;
        this.directory = directory;
    }

    public static TemplateEnum getType(String code) {
        TemplateEnum type = null;
        for (TemplateEnum value : TemplateEnum.values()) {
            if (value.code.equals(code)) {
                type = value;
            }
        }
        return type;
    }

    public static String getDirectory(String code) {
        TemplateEnum type = null;
        for (TemplateEnum value : TemplateEnum.values()) {
            if (value.code.equals(code)) {
                type = value;
                break;
            }
        }
        if (type != null) {
            return type.directory;
        }
        // 默认返回
        String lowerCode = code.toLowerCase();
        if (lowerCode.contains("request")) {
            return "request";
        }
        if (lowerCode.contains("dao")) {
            return "dao";
        }
        if (lowerCode.contains("service")) {
            return "service";
        }
        if (lowerCode.contains("dto")) {
            return "dto";
        }
        if (lowerCode.contains("entity")) {
            return "entity";
        }
        if (lowerCode.contains("repository")) {
            return "repository";
        }
        return "";
    }

    public String getCode() {
        return code;
    }

    public String getDes() {
        return des;
    }

    public String getDirectory() {
        return directory;
    }
}
